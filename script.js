const arrows = document.querySelectorAll(".right");
const galleryList = document.querySelectorAll(".gallery-list");

arrows.forEach((right, i) => {

    let clickCounter = 0;
    right.addEventListener('click', ()=>{
        const itemNum   = galleryList[i].querySelectorAll("img").length;
        const galleryItem = galleryList[i].querySelector(".gallery-list-items")
        
        galleryList[i].appendChild(galleryItem);
    }); 
});